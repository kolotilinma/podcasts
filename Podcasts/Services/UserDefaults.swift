//
//  UserDefaults.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 17.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static let favoritedPodcastKey = "favoritedPodcastKey"
    static let downloadedEpisodesKey = "downloadedEpisodesKey"
        
        func deleteEpisode(episode: Episode) {
            let savedEpisodes = downloadedEpisodes()
            let filteredEpisodes = savedEpisodes.filter { (e) -> Bool in
                // you should use episode.collectionId to be safer with deletes
                return e.title != episode.title
            }
            
            do {
                let data = try JSONEncoder().encode(filteredEpisodes)
                UserDefaults.standard.set(data, forKey: UserDefaults.downloadedEpisodesKey)
            } catch let encodeErr {
                print("Failed to encode episode:", encodeErr)
            }
        }
        
        func downloadEpisode(episode: Episode) {
            do {
                var episodes = downloadedEpisodes()
    //            episodes.append(episode)
                //insert episode at the front of the list
                episodes.insert(episode, at: 0)
                let data = try JSONEncoder().encode(episodes)
                UserDefaults.standard.set(data, forKey: UserDefaults.downloadedEpisodesKey)
                
            } catch let encodeErr {
                print("Failed to encode episode:", encodeErr)
            }
        }
        
        func downloadedEpisodes() -> [Episode] {
            guard let episodesData = data(forKey: UserDefaults.downloadedEpisodesKey) else { return [] }
            
            do {
                let episodes = try JSONDecoder().decode([Episode].self, from: episodesData)
                return episodes
            } catch let decodeErr {
                print("Failed to decode:", decodeErr)
            }
            
            return []
        }
    
    
    func savedPodcasts() -> [Podcast] {
        var podcasts = [Podcast]()
        do {
            guard let data  = UserDefaults.standard.data(forKey: UserDefaults.favoritedPodcastKey) else { return [] }
            podcasts = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! [Podcast]
            return podcasts
        } catch let error {
            print(error.localizedDescription)
            return podcasts
        }
    }
    
    func deletePodcast(podcast: Podcast) {
        
        do {
            let podcasts = savedPodcasts()
            let filteredPodcasts = podcasts.filter { (p) -> Bool in
                return p.trackName != podcast.trackName && p.artistName != podcast.artistName
            }
            let data = try NSKeyedArchiver.archivedData(withRootObject: filteredPodcasts, requiringSecureCoding: false)
            UserDefaults.standard.set(data, forKey: UserDefaults.favoritedPodcastKey)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
