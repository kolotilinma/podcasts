//
//  NetworkService.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 12.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import Alamofire
import FeedKit

extension Notification.Name {
    
    static let downloadProgress = NSNotification.Name("downloadProgress")
    static let downloadComplete = NSNotification.Name("downloadComplete")
}


class NetworkService {
    fileprivate let baseiTunesSearchURL = "https://itunes.apple.com/search"
    static let shared = NetworkService()
    typealias EpisodeDownloadCompleteTuple = (fileUrl: String, episodeTitle: String)
    
    func downloadEpisode(episode: Episode) {
//        print("Downloading episode using Alamofire at stream url:", episode.streamUrl ?? "")
        guard let url = URL(string: episode.streamUrl ?? "") else { return }
        
        let downloadRequest = DownloadRequest.suggestedDownloadDestination()
        
        AF.download(url, interceptor: nil, to: downloadRequest).downloadProgress { (progress) in
//            print(progress.fractionCompleted)
            NotificationCenter.default.post(name: .downloadProgress, object: nil, userInfo: ["title": episode.title, "progress": progress.fractionCompleted])
            
        }.response { (resp) in
//            print(resp.fileURL ?? "")
            let episodeDownloadComplete = EpisodeDownloadCompleteTuple(fileUrl: resp.fileURL?.absoluteString ?? "", episode.title)
            NotificationCenter.default.post(name: .downloadComplete, object: episodeDownloadComplete, userInfo: nil)
            
            var downloadedEpisodes = UserDefaults.standard.downloadedEpisodes()
            guard let index = downloadedEpisodes.firstIndex(where: {$0.streamUrl == episode.streamUrl}) else { return }
            downloadedEpisodes[index].fileUrl = resp.fileURL?.absoluteString
            do {
                let data = try JSONEncoder().encode(downloadedEpisodes)
                UserDefaults.standard.set(data, forKey: UserDefaults.downloadedEpisodesKey)
                
            } catch let error {
                print("Failed to encode dowloaded episodes", error.localizedDescription)
            }
        }
    }
    
    func fetchTracks(searchText: String, completion: @escaping ((Result<SearchResults, Error>)) -> Void) {
        let parameters = ["term":"\(searchText)", "limit":"50", "media":"podcast"]
        AF.request(baseiTunesSearchURL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            if let error = dataResponse.error {
                completion(.failure(error))
                return
            }
            guard let data = dataResponse.data else { return }
            do {
                let searchResult = try JSONDecoder().decode(SearchResults.self, from: data)
                completion(.success(searchResult))
            } catch let error {
                completion(.failure(error))
            }
        }
    }
    
    func fetchPodcasts(searchText: String, completion: @escaping ((Result<[Episode], Error>)) -> Void) {
        guard let url = URL(string: searchText.toSecureHTTPS()) else { return }
        DispatchQueue.global(qos: .background).async {
            let parser = FeedParser(URL: url)
            parser.parseAsync { (result) in
                switch result {
                case .success(let feed):
                    switch feed {
                    case let .rss(feed):
                        completion(.success(feed.toEpisodes()))
                        break
                    default:
                        break
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    
    struct SearchResults: Decodable {
        let resultCount: Int
        let results: [Podcast]
    }
    
}
