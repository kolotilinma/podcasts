//
//  PodcastSearchController.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 12.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class PodcastSearchController: UITableViewController {
    
    var podcasts: [Podcast] = []
    private var timer: Timer?
    private lazy var footerView = FooterView()
    let cellId = "cellId"
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        view.backgroundColor = .mainWhite()
        setupSearchBar()
        setupTableView()
        
//        searchBar(searchController.searchBar, textDidChange: "Voong")
    }
    
    private func setupSearchBar() {
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.searchBar.delegate = self
    }
    
    private func setupTableView() {
        tableView.tableFooterView = footerView
        let nib = UINib(nibName: "TrackCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: cellId)
    }
    
}

//MARK: - SearchBar Delegate
extension PodcastSearchController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        podcasts = []
        tableView.reloadData()
        footerView.showLoader()
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (_) in
            NetworkService.shared.fetchTracks(searchText: searchText) { (result) in
                switch result {
                case .success(let searchResult):
                    self.podcasts = searchResult.results
                    self.footerView.hideLoader()
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error.localizedDescription)
                    self.footerView.hideLoader()
                }
            }
        })
    }
}

//MARK: - TableView
extension PodcastSearchController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return podcasts.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.text = "Please enter a Search Term"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textColor = .purple
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.podcasts.count > 0 ? 0 : 250
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TrackCell
        let podcast = self.podcasts[indexPath.row]
        cell.podcast = podcast
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let episodesVC = EpisodesController()
        let podcast = self.podcasts[indexPath.row]
        episodesVC.podcast = podcast
        navigationController?.pushViewController(episodesVC, animated: true)
    }
    
}
