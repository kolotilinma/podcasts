//
//  Episode.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 13.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation
import FeedKit

struct Episode: Codable {
    let title: String
    let author: String
    let pubDate: Date
    let description: String
    let streamUrl: String?
    var imageUrl: String?
    var fileUrl: String?
    
    init(feedItem: RSSFeedItem) {
        self.title = feedItem.title ?? ""
        self.author = feedItem.iTunes?.iTunesAuthor ?? ""
        self.streamUrl = feedItem.enclosure?.attributes?.url ?? ""
        self.pubDate = feedItem.pubDate ?? Date()
        self.description = feedItem.iTunes?.iTunesSubtitle ?? feedItem.description ?? ""
        self.imageUrl  = feedItem.iTunes?.iTunesImage?.attributes?.href
    }
}
