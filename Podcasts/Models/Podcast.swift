//
//  Podcast.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 12.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation

class Podcast: NSObject, Decodable, NSCoding {
    
    var trackName: String?
    var artistName: String?
    var artworkUrl600: String?
    var trackCount: Int?
    var feedUrl: String?
    
    func encode(with coder: NSCoder) {
//        print("Trying to transform Podcast into Data")
        coder.encode(trackName, forKey: "trackNameKey")
        coder.encode(artistName, forKey: "artistNameKey")
        coder.encode(artworkUrl600, forKey: "artworkUrl100Key")
        coder.encode(trackCount, forKey: "trackCountKey")
        coder.encode(feedUrl, forKey: "feedUrlKey")
    }
    
    required init?(coder: NSCoder) {
//        print("Trying to torn Data  into Podcast")
        self.trackName = coder.decodeObject(forKey: "trackNameKey") as? String
        self.artistName = coder.decodeObject(forKey: "artistNameKey") as? String
        self.artworkUrl600 = coder.decodeObject(forKey: "artworkUrl100Key") as? String
        self.trackCount = coder.decodeObject(forKey: "trackCountKey") as? Int
        self.feedUrl = coder.decodeObject(forKey: "feedUrlKey") as? String
    }
    
}
