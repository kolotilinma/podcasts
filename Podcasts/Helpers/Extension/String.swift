//
//  String.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 13.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import Foundation

extension String {
    func toSecureHTTPS() -> String {
        return self.contains("https") ? self : self.replacingOccurrences(of: "http", with: "https")
    }
}
