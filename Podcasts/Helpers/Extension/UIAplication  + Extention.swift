//
//  UIAplication  + Extention.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 15.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

extension  UIApplication {
    static  func  mainTabBarController() -> MainTabBarController? {
        return shared.windows.filter { $0.isKeyWindow }.first?.rootViewController as? MainTabBarController
    }
}
