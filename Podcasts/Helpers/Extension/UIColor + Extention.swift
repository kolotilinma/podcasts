//
//  UIColor + Extention.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 12.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func buttonWhite() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)
            }
        }
    }
    static func mainWhite() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.1154643521, green: 0.1147854105, blue: 0.1159910634, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0.968627451, green: 0.9725490196, blue: 0.9921568627, alpha: 1)
            }
        }
    }
    static func mainDark() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.1647058824, green: 0.1725490196, blue: 0.2117647059, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
    }
    static func stakViewColor() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.1647058824, green: 0.1725490196, blue: 0.2117647059, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
    }
    static func ordinaryText() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    static func textLabelGray() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.6705882353, green: 0.7058823529, blue: 0.7411764706, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            }
        }
    }
    
    static func textLabelColor() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    static func primaryColor() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 0.937254902, green: 0.2117647059, blue: 0.3176470588, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 0.937254902, green: 0.2117647059, blue: 0.3176470588, alpha: 1)
            }
        }
    }
    
    static func errorColor() -> UIColor {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                /// Return the color for Dark Mode
                return #colorLiteral(red: 1, green: 0.1411764706, blue: 0.1411764706, alpha: 1)
            } else {
                /// Return the color for Light Mode
                return #colorLiteral(red: 1, green: 0.1411764706, blue: 0.1411764706, alpha: 1)
            }
        }
    }
    
}
