//
//  ViewController.swift
//  Podcasts
//
//  Created by Михаил Колотилин on 12.05.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var customView: UIView!
    
    override func loadView() {
        view = UIView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        view.backgroundColor = .red
        self.customView = UIView(frame: .init(x: 100, y: 250, width: 250, height: 250))
        self.customView.backgroundColor = .red
        view.addSubview(customView)
//        transfer(self.customView)
    }

    func transfer(_ view: UIView) {
        // Remove the view from the UIViewController.
        view.removeFromSuperview()
        // Add the view to the UIWindow.
        UIApplication.shared.windows.first!.addSubview(view)
    }

}

